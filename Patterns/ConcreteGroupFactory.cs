﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Patterns
{
    public class ConcreteGroupFactory : AbstractGroupFactory
    {
        protected override IGroup makeGroup(string type)
        {
            switch (type)
            {
                case "day":
                    return new DayGroup();
                case "week":
                    return new WeekGroup();
                case "month":
                    return new MonthGroup();
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
