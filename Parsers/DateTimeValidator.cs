﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Parsers
{
    public static class DateTimeValidator
    {

        public static bool isValidDate(string date) {
            DateTime dateValue;
            bool isParseblaeDate = false;
            //try to parse DateTime
            if (DateTime.TryParse(date, out dateValue))
            {
                isParseblaeDate = true;
            }
            return isParseblaeDate;
        }
        public static bool isValidDate(string start, string end)
        {
            bool isParseblaeDate = isValidDate(start) && isValidDate(end);
            return isParseblaeDate;
        }

    }
}
