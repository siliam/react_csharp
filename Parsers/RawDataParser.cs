﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using react_cSharp_restAPI.Models;

namespace react_cSharp_restAPI.Parsers
{
    public class RawDataParser
    {
        public IEnumerable<HourConsumptionBeforeParse> hourConsumptionBeforeParse { get; set; }
        DateTime start;
        DateTime end;
        public RawDataParser(DateTime start, DateTime end, IEnumerable<HourConsumptionBeforeParse> hourConsumptionBeforeParse)
        {
            this.start = start;
            this.end = end;
            this.hourConsumptionBeforeParse = hourConsumptionBeforeParse;
        }

        public List<HourConsumption> parseRawData()
        {
            List<HourConsumption> parsedData = new List<HourConsumption>();
            //loop
            Parallel.ForEach(this.hourConsumptionBeforeParse, (HourConsumptionBeforeParse hcbp) =>
            {
                string kWh = DoubleValidator.formatDouble(hcbp.kwh.ToString());
                //try parse date and double
                //try parse double
                double KWHnumber;
                if (Double.TryParse(kWh, out KWHnumber))
                {
                    //try parse date 
                    string dateString = hcbp.date;
                    DateTime dateValue;
                    if (DateTime.TryParse(dateString, out dateValue))
                    {
                        //datetime should fall into given time slot
                        if (dateValue >= start && dateValue <= end)
                        {
                            HourConsumption hc = new HourConsumption(dateValue, KWHnumber);
                            parsedData.Add(hc);
                        }
                    }
                }
            });
            return parsedData;
        }
    }
}

