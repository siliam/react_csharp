﻿import * as KWHcontroller from  '../controllers/KWHcontroller.js';
import apiRouteNames from "../app_identities/api_route_names";
import app_data from '../app_identities/app_data'

export default class AccessToContr {
    constructor(self, startDate, endDate, group_index) {
        this.self = self;
        this.startDate = startDate;
        this.endDate = endDate;
        this.group_index = group_index;
    }

    fetchData() {
        var KWHroute = apiRouteNames.kwH_url;
        let grouping_options = app_data.select_grouping_EN.join(":");
        KWHroute += "/" + this.startDate + "/" + this.endDate + "/" + this.group_index + "/" + grouping_options
        console.log("AccessController fetchData() KWHroute", KWHroute);
        KWHcontroller.getKWHdata(this.self, KWHroute);
    };
   
}