﻿/*jshint esversion: 6 */
import axios from 'axios';


export function getKWHdata(self, KWHroute) {
    console.log('CONTROLLER GET getKWHdata fetched_data', KWHroute);
    axios.get(KWHroute)
        .then(function (response) {
            var fetched_data = response.data;
            console.log('GET getKWHdata fetched_data', fetched_data);
            //sort by order
            if (fetched_data) {
                self.setState({ fetched_KWH: fetched_data });
            }
        })
        .catch(function (error) {
            console.log('GET getKWHdata fetched_data', error);
        });
}