import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import StateValueGetter from './app_identities/state_values'


const hasProps = injectedProps => WrappedComponent => {
    const HasProps = props => <WrappedComponent {...injectedProps} {...props} />
    return HasProps
}

export default class App extends Component {
    displayName = App.name

    constructor(props) {
        super(props);
        this.state = {
            startDate: '',
            endDate: '',
            price: 1,
            grouping: 0
        }
        this.handleStateChange = this.handleStateChange.bind(this);
        this.ComponentFactory = this.ComponentFactory.bind(this);
    }

    componentDidMount() {
        var stateGetter = new StateValueGetter();
        this.setState({
            startDate: stateGetter.start_date,
            endDate: stateGetter.end_date,
            price: stateGetter.price_,
            grouping: stateGetter.group,
        });
    }


    handleStateChange(price, start, end, grouping) {
        this.setState({
            startDate: start,
            endDate: end,
            price: price,
            grouping: grouping,
        });
        console.log("this.state.price APP handlePriceChange(_price)", price);
    }


    ComponentFactory(componentName) {
        let start = this.state.startDate;
        let end = this.state.endDate;
        let price = this.state.price;
        let grouping = this.state.grouping;
        if (componentName.toLowerCase() === "home")
            return hasProps({ price: price, start:start, end: end, group:grouping })(Home);
    }


    render() {

        let start = this.state.startDate;
        let end = this.state.endDate;
        let price = this.state.price;
        let grouping = this.state.grouping;
        //console.log("APP PRICE:", price);
        let HomeCompo = this.ComponentFactory("home");

    return (
        <Layout
            price={price}
            end={end}
            start={start}
            grouping={grouping}
            handleStateChange={this.handleStateChange}
        >
            <Route exact path='/' component={HomeCompo} />
        </Layout>
    );
  }
}
