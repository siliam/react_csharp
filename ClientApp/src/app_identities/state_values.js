﻿import app_data_identity from '../app_identities/app_data_identity'
import DateProcessor from '../utils/DateProcessor'
import UserChoice from '../utils/UserChoice'


export default class StateValueGetter {
    constructor() {
        let startDate_sess = UserChoice.readDate(app_data_identity.startdate);
        let endDate_sess = UserChoice.readDate(app_data_identity.enddate);
        let price_sess = parseFloat(UserChoice.read(app_data_identity.price));
        let group_sess = parseInt(UserChoice.read(app_data_identity.group), 10);
        //console.log('startDate_sess', startDate_sess);
        //console.log('endDate_sess', endDate_sess);
        //console.log('price_sess', price_sess);
        //console.log('group_sess', group_sess);
        this.start_date = startDate_sess ? startDate_sess : DateProcessor.getEarliestDateString();
        this.end_date = endDate_sess ? endDate_sess : DateProcessor.formatDate(new Date());
        this.price_ = price_sess ? price_sess : 1;
        this.group = group_sess ? group_sess : 0;
    }

}