﻿const app_data = {
    select_dates_guide_ET: "Vali algus- ja lõppkuupäev",
    select_dates_startdate_ET: "ALGUSKUUPÄEV",
    select_dates_enddate_ET: "LÕPPKUUPÄEV",
    select_grouping_guide_ET: "Vali grupp",
    select_grouping_ET: ['päev', 'nädal', 'kuu'],
    select_grouping_EN: ['day', 'week', 'month'],
    select_price_guide_ET: "Vali kW/h hind",
}

export default app_data;