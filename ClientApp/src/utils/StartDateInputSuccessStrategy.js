﻿import DateProcessor from '../utils/DateProcessor'
import UserChoice from '../utils/UserChoice'
import data_identity from '../app_identities/app_data_identity'

export default class EndDateErrorStrategy {
    constructor(self, date, isValidDate) {
        this.self = self;
        this.date = date;
        this.isValidDate = isValidDate;
    }

    setSuccess() {
        if (this.isValidDate) {
            this.self.setState({ startDate: DateProcessor.formatDate(this.date) });
            UserChoice.save(data_identity.startdate, this.date);
        }
    };
}