﻿import messages from '../utils/Messages'

export default class StartDateErrorStrategy {
    constructor(self, dateIsInLimits) {
        this.self = self;
        this.dateIsInLimits = dateIsInLimits;
    }

    setError() {
        let isValidDate = false;
        if (this.dateIsInLimits) {
            this.self.setState({ startDateError: '' });
            isValidDate = true;
        }
        else {
            this.self.setState({ startDateError: messages.wrongStartDate_ET });
        }
        return isValidDate;
    };
}