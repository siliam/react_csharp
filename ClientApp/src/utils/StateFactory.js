﻿import DateStateFactory from '../utils/DateStateFactory'
import data_identity from '../app_identities/app_data_identity'

export default class StateFactory {
    constructor(self, name, value) {
        this.self = self;
        this.name = name;
        this.value = value;
        this.setConcreteFactory();
    }

    setConcreteFactory() {
        if (this.name === data_identity.startdate || this.name === data_identity.enddate) {
            this.concreteFactory = new DateStateFactory(this.self, this.name, this.value);
            console.log('is DateStateFactory value', this.value);
        }
        console.log('is StateFactory.setConcreteFactory()');
    }
    
    validate() {
        this.concreteFactory.validateState();
    };
}




