﻿const messages = {
    wrongStartDate_ET : "Alguskuupäev ei tohi olla varasem, kui kaks aastat tagasi ega tulevikus.",
    wrongEndDate_ET : "Lõppkuupäev ei tohi olla tulevikus ega varasem, kui kaks aastat tagasi.",
    startDateIsSmallerThanEndDate_ET : "Alguskuupäev peab olema varasem või sama kui lõppkuupäev.",
}
export default messages;