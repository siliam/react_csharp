﻿import DateValidator from '../utils/DateValidator'
import StartDateErrorStrategy from '../utils/StartDateErrorStrategy'
import EndDateErrorStrategy from '../utils/EndDateErrorStrategy'
import DateErrorStrategy from '../utils/DateErrorStrategy'
import StartDateInputSuccessStrategy from '../utils/StartDateInputSuccessStrategy'
import EndDateInputSuccessStrategy from '../utils/EndDateInputSuccessStrategy'
import data_identity from '../app_identities/app_data_identity'

export default class DateStateFactory {
    constructor(self, name, date) {
        this.self = self;
        this.name = name;
        this.date = date;
        this.milliseconds = date.getTime()
        this.defineStrategies();
    }


    /**
    *input type: string
    *@strDate date is in format "2016-07-04"
    *@returns date is in format "2016-07-04"
    *@returns type Date
    **/
    getDateInMilleiecondsFromString(strDate) {
        return (new Date(strDate)).getTime();
    }

    defineStrategies() {
        this.stateStartDate = this.getDateInMilleiecondsFromString(this.self.state.startDate);
        this.stateEndDate = this.getDateInMilleiecondsFromString(this.self.state.endDate);
        console.log('DateStateFactory.defineStrategies()');
        console.log('this.name', this.name);
        console.log('this.date', this.date);
        console.log('this.milliseconds', this.milliseconds);
        console.log('this.self.state', this.self.state);
        console.log('this.stateStartDate', this.stateStartDate);
        console.log('this.stateEndDate', this.stateEndDate);
        if (this.name === data_identity.startdate) {
            this.dateValidator = new DateValidator(this.milliseconds, this.stateEndDate);
            let isStartInValidLimits = this.dateValidator.isStartInValidLimits();
            this.errorStrategy = new StartDateErrorStrategy(this.self, isStartInValidLimits);
            this.successStrategy = new StartDateInputSuccessStrategy(this.self, this.date, isStartInValidLimits);
        }
        else if (this.name === data_identity.enddate) {
            this.dateValidator = new DateValidator(this.stateStartDate, this.milliseconds);
            let isEndInValidLimits = this.dateValidator.isEndInValidLimits();
            this.errorStrategy = new EndDateErrorStrategy(this.self, isEndInValidLimits);
            this.successStrategy = new EndDateInputSuccessStrategy(this.self, this.date, isEndInValidLimits);
        }
        let isStartSmallerThanEnd = this.dateValidator.isStartSmallerOrEqualToEnd();
        this.dateErrorStrategy = new DateErrorStrategy(this.self, this.errorStrategy, isStartSmallerThanEnd);
    }

    validateState() {
        this.dateErrorStrategy.setErrors();
        this.successStrategy.setSuccess();
    }
    
}




