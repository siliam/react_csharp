﻿import messages from '../utils/Messages'

export default class EndDateErrorStrategy {
    constructor(self, dateIsInLimits) {
        this.self = self;
        this.dateIsInLimits = dateIsInLimits;
    }

    setError() {
        let isValidDate = false;
        if (this.dateIsInLimits) {
            this.self.setState({ endDateError: '' });
            isValidDate = true;
        }
        else {
            this.self.setState({ endDateError: messages.wrongEndDate_ET });
        }
        return isValidDate;
    };
}