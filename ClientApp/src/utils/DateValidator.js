﻿import DateProcessor from '../utils/DateProcessor'

export default class DateValidator {
    constructor(start, end) {
        this.start = start;
        this.end = end;
        this.setLimits();
    }

    setLimits() {
        //earliest valid time (twoYearsAgo)
        this.minTime = DateProcessor.getEarliestDate().getTime();
    }

    isStartInValidLimits() {
        return this.isInValidLimits(this.start);
    }

    isEndInValidLimits() {
        return this.isInValidLimits(this.end);
    }

    isInValidLimits(date) {
        console.log('DateValidator.isInValidLimits(date)')
        let isValidDate = false;
        let currentDate = (new Date()).getTime();
        console.log('this.minTime', this.minTime);
        console.log('date', date);
        console.log('currentDate', currentDate);
        if (date >= this.minTime && date <= currentDate) {
            isValidDate = true;
        }
        console.log('isValidDate', isValidDate)
        return isValidDate;
    }

    isStartSmallerOrEqualToEnd() {
        console.log('DateValidator.isStartSmallerOrEqualToEnd')
        console.log('this.start', this.start);
        console.log('this.end', this.end);
        let isValidDate = false;
        if (this.start <= this.end) {
            isValidDate = true;
        }
        console.log('isStartSmallerThanEnd', isValidDate);
        return isValidDate;
    }
}




