﻿import messages from '../utils/Messages'


export default class DateErrorStrategy {
    constructor(self, concreteDateErrorStrategy, isStartSmallerOrWqualToEnd) {
        this.self = self;
        this.concreteDateErrorStrategy = concreteDateErrorStrategy;
        this.isStartSmallerOrWqualToEnd = isStartSmallerOrWqualToEnd;
    }

    setErrors() {
        console.log('DateErrorStrategy.setErrors() ');
        console.log('this.isStartSmallerOrWqualToEnd', this.isStartSmallerOrWqualToEnd);
        if (this.isStartSmallerOrWqualToEnd) {
            console.log('self.setState: ------ start is before end or equal');
            this.self.setState({ endDateIsBeforeStartDateError: '' });
        }
        else {
            console.log('self.setState: ------ start is NOT before end ');
            this.self.setState({ endDateIsBeforeStartDateError: messages.startDateIsSmallerThanEndDate_ET });
        }
        console.log('self.state', this.self.state);
        this.concreteDateErrorStrategy.setError();
    };
}