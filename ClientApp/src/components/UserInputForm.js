﻿import React, { Component } from 'react';
import StateFactory from '../utils/StateFactory'
import UserChoice from '../utils/UserChoice'
import app_data_identity from '../app_identities/app_data_identity'
import app_data from '../app_identities/app_data'
import StateValueGetter from '../app_identities/state_values'
import '../css/user_form.css';

export class UserInputForm extends Component {
    displayName = UserInputForm.name

    constructor(props) {
        super(props);
        this.state = {
            startDate: '',
            endDate: '',
            price: 1,
            grouping: 0,
            endDateIsBeforeStartDateError: '',
            startDateError: '',
            endDateError: '',
            fetched_KWH : ''
        };
        this.datePickerChanged = this.datePickerChanged.bind(this);
        this.getGroupOptionsList = this.getGroupOptionsList.bind(this);
        this.priceChanged = this.priceChanged.bind(this);
        this.groupChanged = this.groupChanged.bind(this);
    }

    componentDidMount() {
        var stateGetter = new StateValueGetter();
        this.setState({
            startDate: stateGetter.start_date,
            endDate: stateGetter.end_date,
            price: stateGetter.price_,
            grouping: stateGetter.group,
        });
    }

    datePickerChanged(evt) {
        console.log('datePickerChanged');
        if (evt) {
            evt.preventDefault();
            let name = evt.target.name;
            console.log('datePickerChanged target.name', name);
            let value = new Date(evt.target.value);
            console.log('datePickerChanged target.value', value);
            let stateFactory = new StateFactory(this, name, value);
            stateFactory.validate();
        }
    }

    priceChanged(evt) {
        console.log('priceChanged');
        if (evt) {
            evt.preventDefault();
            let value = parseFloat(evt.target.value);
            this.setState({ price: value });
            UserChoice.save(app_data_identity.price, value);
            this.props.handleStateChange(value, this.state.startDate, this.state.endDate, this.state.grouping );

        }
    }

    groupChanged(evt) {
        console.log('groupChanged');
        if (evt) {
            evt.preventDefault();
            let value = parseInt(evt.target.value, 10);
            console.log('evt.target', evt.target);
            console.log('evt.target.value', evt.target.value);
            this.setState({ grouping: value });
            UserChoice.save(app_data_identity.group, value);
        }
    }

    getGroupOptionsList() {
        let grouping_options = app_data.select_grouping_ET;
        let opt_rows = [];
        let counter = 0;

        for (let opt of grouping_options) {
            opt_rows.push(<option key={counter} value={counter} defaultValue>{opt}</option>);
            counter++;
        }
        return opt_rows;
    }

    render() {

        //user interactions (Input values)
        let startDate = this.state.startDate;
        //console.log('startDate', startDate);
        let endDate = this.state.endDate;
        //console.log('endDate', endDate);
        let opt_rows = this.getGroupOptionsList();


        console.log("USERINPUT price , navmenuUupper_price", this.props.price);

        return (
            <div className="user_form_container">
                <fieldset>
                    <legend>{app_data.select_dates_guide_ET + ":"}</legend>
                    <div className="date_picker_error">{this.state.endDateIsBeforeStartDateError}</div>

                    <div className="date_picker_guide">{app_data.select_dates_startdate_ET + ":"}</div>
                    <div className="date_picker">
                        <input
                            type="date"
                            id="start"
                            placeholder={app_data.select_dates_startdate_ET}
                            name={app_data_identity.startdate}
                            onChange={this.datePickerChanged}
                            value= {startDate}
                        />
                    </div>
                    <div className="date_picker_error">{this.state.startDateError}</div>

                    <div className="date_picker_guide">{app_data.select_dates_enddate_ET + ":"}</div>
                    <div className="date_picker">
                        <input
                            type="date"
                            id="end"
                            placeholder={app_data.select_dates_enddate_ET}
                            name={app_data_identity.enddate}
                            onChange={this.datePickerChanged}
                            value={endDate}
                        />
                    </div>
                    <div className="date_picker_error">{this.state.endDateError}</div>
                </fieldset>
                <fieldset>
                    <legend>{app_data.select_grouping_guide_ET + ":"}</legend>
                    <div className="date_picker">
                        <select
                            value={this.state.grouping} 
                            onChange={this.groupChanged}>
                            {opt_rows}
                        </select>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{app_data.select_price_guide_ET + ":"}</legend>
                    <div className="date_picker">
                        <input
                            type="number"
                            id="price"
                            name={app_data_identity.price}
                            onChange={this.priceChanged}
                            value={this.state.price}
                            min="0"
                        />
                    </div>
                </fieldset>
            </div>
        );
    }
}
