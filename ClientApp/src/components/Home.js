import React, { Component } from 'react';
import DateProcessor from '../utils/DateProcessor'
import AccessToContr from '../controllers/accessToController'
import app_data from '../app_identities/app_data'
import '../css/home.css';

export class Home extends Component {
    displayName = Home.name
    constructor(props) {
        super(props);
        this.state = {
            fetched_KWH: ''
        }
        this.fetchData = this.fetchData.bind(this);
        this.displayData = this.displayData.bind(this);
        this.makeRows = this.makeRows.bind(this);
    }

    componentWillMount() {
        console.log("HOME STATE: this.mounted componentWillMount", this.props);
        let start = this.props.start;
        let end = this.props.end;
        let price = this.props.price;
        let group = this.props.group;
        this.fetchData(start, end, group);
    }
    componentDidMount() {
    }

    displayData(fetched_data) {
       
        if (typeof fetched_data === typeof ''){//error
            return fetched_data;
        }
        let rows = this.makeRows(fetched_data);
        return (
            <div className='table'>
                    {rows.map((row, i) => 
                    <div key={i}>
                            <div>{row[i]}</div>
                        </div>
                    )}
            </div>
        );
    }


    makeRows(fetched_data) {
        if (typeof fetched_data === typeof '') {//error
            return fetched_data;
        }

        let rows = [];
        let innerRows = [];
        let group_type = app_data.select_grouping_ET[this.props.group];
        innerRows.push(
            <div className="cellrow" key={"head"}>
                <div className="cell head">{group_type}</div>
                <div className="cell head">vahe</div>
                <div className="cell head">kWh</div>
                <div className="cell head">hind</div>
                <div className="clear"></div>
            </div>
        );
        let k_count = 0;
        for (var key in fetched_data) {
            k_count++;
            if (fetched_data.hasOwnProperty(key)) {
                let data = fetched_data[key];
                let datetimes = "";
                if (data["dateTimes"]) {
                    data["dateTimes"].map((dt, j) =>
                        datetimes += (j > 0) ? (dt.split("T")[0]) : (dt.split("T")[0] + " - " )
                    )
                }
                let kWh = Math.round(parseFloat(data["sumOfKwh"]) * 100) / 100;
                let price = Math.round(kWh * parseInt(this.props.price) * 100) / 100;
                innerRows.push(
                    <div className="cellrow" key={k_count}>
                        <div className="cell">{key}</div>
                        <div className="cell">{datetimes}</div>
                        <div className="cell">{kWh}</div>
                        <div className="cell">{price}</div>
                        <div className="clear"></div>
                    </div>
                );
                rows.push(innerRows);
            }
        }
        return rows;
    }

    fetchData(start, end, group) {
        if (start && end) {
            var a = new AccessToContr(this, start, end, group);
            a.fetchData();
        }
    }

    render() {

        //console.log("HOME  this.props.price:", price);
        //console.log("HOME  this.props.start:", start);
        //console.log("HOME  this.props.end:", end);
        //console.log("HOME  this.props.group:", group);

        const f = this.displayData(this.state.fetched_KWH);
        //console.log("HOME STATE: this.mounted", f );
        
      return (
        
          <div>
              <h1>Elektrikulu arvutamine</h1>
              <h1>Hind: {this.props.price}</h1>
                <br />
                  {f}
          </div>
    );
  }
}
