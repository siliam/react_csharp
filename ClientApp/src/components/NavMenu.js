﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { UserInputForm } from './UserInputForm';
import '../css/NavMenu.css';


export class NavMenu extends Component {
  displayName = NavMenu.name


  render() {
    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
                    <Link to={'/'}>
                        <span className="navbar_brand">
                            KW/h
                        </span>
                    </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <UserInputForm
                price={this.props.price}
                handleStateChange={this.props.handleStateChange}
         />
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem >
                   <Glyphicon glyph='th-list' /> Tabeli vaade
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
