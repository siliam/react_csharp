﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using react_cSharp_restAPI.Models;
using react_cSharp_restAPI.Parsers;
using react_cSharp_restAPI.Patterns;

namespace react_cSharp_restAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/KiloWattData")]
    public class KiloWattDataController : Controller
    {
        string errorMessage = "Error: invalid data";

        // GET: api/KiloWattData/5/6
        [HttpGet("{start}/{end}/{group_index}/{grouparray}", Name = "Get")]
        public Object  Get(string start, string end, int group_index, string grouparray)
        {
            string[] arr = grouparray.Split(':');
            group_index = group_index > arr.Length ? arr.Length - 1 : (group_index < 0 ? 0 : group_index);
            //get group type string
            string grp = arr[group_index];
            //are date time strings parseble
            if (!DateTimeValidator.isValidDate(start, end))
            {
                return HttpStatusCode.BadRequest;
            }
            //parse dates
            DateTime startDateTime = DateTime.Parse(start);
            DateTime endDateTime = DateTime.Parse(end);

            //construct request url
            string url = "https://finestmedia.ee/kwh/?start=" + start + "&end=" + end;

            //fetch remote data
            WebClient client = new WebClient();
            client.Headers["Accept"] = "application/xml";
            //returned data
            string returnedString = client.DownloadString(new Uri(url));
            try
            {
                //parse hour consumption from XML data
                XElement root = XElement.Parse(returnedString);

                //collect data into HourConsumptionBeforeParse
                //in order to try parse its values later
                var q = from history in root.Descendants("ConsumptionHistory")
                            //where artikel.Element("ID").Value.Equals("15")
                        from element in history.Elements()
                        select new HourConsumptionBeforeParse(element.Attribute("ts").Value, element.Value.Replace(".", ","));


                RawDataParser rawParser = new RawDataParser(startDateTime, endDateTime, q);
                //stores parsed data
                List<HourConsumption> hourConsumption = rawParser.parseRawData();
                //fctory pattern for producing a group by group name
                AbstractGroupFactory groupFactory = new ConcreteGroupFactory();
                IGroup concreteGroup = groupFactory.getObject(grp);
                //int is number of  month, week or day of a year
                //innergroup sums up KWH values and stores dates for a group
                IDictionary<string, InnerGroup> groupedData = concreteGroup.groupData(hourConsumption);
                return groupedData;
            }
            catch (XmlException e)
            {
                if(returnedString.Length <100)
                    return returnedString;
                return errorMessage + "\n " + e;
            }
            catch (Exception e)
            {
                return errorMessage + "\n " + e;
            }

        }

        // POST: api/KiloWattData
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/KiloWattData/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
