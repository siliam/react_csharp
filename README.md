
## Electricity consumption prices example

Example work for ectricity consumption and its prices, where input of time can be from  2 years ago till yesterday.
The periods of time are month, week and day. Additionally to inputting of start/end time and period, user can insert price for kilowatt hour.
Used human language is Estonian, so:

1. *p�ev* = day

2. *n�dal* = week

3. *kuu* = month

---

## Technologies

Frontend

1. React.js


Backend API

1. C#


---
